package com.lxj.leetcode.simulation;

import com.lxj.algorithm.map.Map;

import java.util.HashMap;

/**
 * @author lee
 * @since 2023/2/19
 */
public class MinimumOperationsToReduceAnIntegerToZero6365 {

    public int minOperations(int n) {
        int count = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < 17; i++) {
            map.put(i, (int)Math.pow((double) 2,i));
        }
        return count;
    }

    public static void main(String[] args) {
        MinimumOperationsToReduceAnIntegerToZero6365 solution = new MinimumOperationsToReduceAnIntegerToZero6365();
        solution.minOperations(54);
        int [][] ints = {
                {0,1,2},
                {3,4,5},
                {6,7,8}
        };
    }
}
