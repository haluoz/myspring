package com.lxj.leetcode.simulation;

import java.util.HashSet;

/**
 * @author lee
 * @since 2023/3/12
 */
public class CountTheNumberOfVowelStringsInRange6315 {

    public int vowelStrings(String[] words, int left, int right) {
        int ans = 0;
        int n = words.length;
        HashSet<Character> characters = new HashSet<>();
        characters.add('a');
        characters.add('e');
        characters.add('i');
        characters.add('o');
        characters.add('u');
        for (int i = left; i <= right && i < n; i++) {
            String word = words[i];
            char starts = word.charAt(0);
            char ends = word.charAt(word.length()-1);
            if (characters.contains(starts) && characters.contains(ends)){
                ans++;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        CountTheNumberOfVowelStringsInRange6315 solution = new CountTheNumberOfVowelStringsInRange6315();
        String[] strings = {"are", "amy", "u"};
        System.out.println(solution.vowelStrings(strings, 0, 2));
    }
}
