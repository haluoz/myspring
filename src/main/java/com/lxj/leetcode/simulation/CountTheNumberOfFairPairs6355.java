package com.lxj.leetcode.simulation;

/**
 * @author lee
 * @since 2023/2/12
 */
public class CountTheNumberOfFairPairs6355 {
    public long countFairPairs(int[] nums, int lower, int upper) {
        long ans = 0;
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            int numI = nums[i];
//            if (numI > upper){
//                continue;
//            }
            for (int j = i+1; j < n; j++) {

                int numJ = nums[j];
                if (lower <= (numI+numJ) && (numI+numJ) <= upper){
//                    System.out.println(numI + ":" + numJ);
                    ans++;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        CountTheNumberOfFairPairs6355 solution = new CountTheNumberOfFairPairs6355();
        System.out.println(solution.countFairPairs(new int[]{-5,-7,-5,-7,-5},
        -12, -12));
    }
}
