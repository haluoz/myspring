package com.lxj.leetcode.simulation;

/**
 * @author lee
 * @since 2023/3/5
 */
public class PassThePillow6307 {
    public int passThePillow(int n, int time) {
        int ans = 0;
        for (int i = 1; i <= n; i++) {
            if (i == n){
                int j = 0;
                for (j = i; j > 1; j--){
                    if (time == 0) break;
                    time--;
                    if (time == 0) {
                        j--;
                        break;
                    }
                }
                i = j;
            }
            if (time == 0) {
                ans = i;
                break;
            }
            time--;
        }
        return ans;
    }

    public static void main(String[] args) {
        PassThePillow6307 solution = new PassThePillow6307();
        System.out.println(solution.passThePillow(3, 2));
//        System.out.println(solution.passThePillow(4, 5));
    }
}
