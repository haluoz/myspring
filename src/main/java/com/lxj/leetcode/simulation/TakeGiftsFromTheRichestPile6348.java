package com.lxj.leetcode.simulation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author lee
 * @since 2023/2/5
 */
public class TakeGiftsFromTheRichestPile6348 {
    public long pickGifts(int[] gifts, int k) {
        long ans = 0;
        int n = gifts.length;
        while(k > 0){
            Arrays.sort(gifts);
            k--;
            int val = (int)Math.sqrt(gifts[n - 1]);
            gifts[n - 1] = val;
        }
        for(int i = 0;i < n;i++){
            ans += gifts[i];
        }

        return ans;
    }

    public long pickGifts1(int[] gifts, int k) {
        long ans = 0;
        int n = gifts.length;
        PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        for (int i = 0; i < n; i++) {
            queue.offer(gifts[i]);
        }
        for (int i = 0; i < k; i++) {
            Integer poll = queue.poll();
            int sqrt = (int) Math.sqrt(poll);
            queue.add(sqrt);
        }
        while (!queue.isEmpty()) {
            ans+= queue.poll();
        }
        return ans;
    }

    public static void main(String[] args) {
        TakeGiftsFromTheRichestPile6348 solution = new TakeGiftsFromTheRichestPile6348();
//        System.out.println(solution.pickGifts(new int[]{25, 64, 9, 4, 100}, 4));
        System.out.println(solution.pickGifts1(new int[]{25, 64, 9, 4, 100}, 4));

    }
}
