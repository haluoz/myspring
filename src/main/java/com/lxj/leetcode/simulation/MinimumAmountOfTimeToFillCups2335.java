package com.lxj.leetcode.simulation;

import java.util.Arrays;

/**
 * @author lee
 * @since 2023/2/11
 */
public class MinimumAmountOfTimeToFillCups2335 {
    public int fillCups(int[] amount) {
        int ans = 0;
        while (amount[0] + amount[1] + amount[2] > 0) {
            Arrays.sort(amount);
            ++ans;
            amount[2]--;
            amount[1] = Math.max(0, amount[1] - 1);
        }
        return ans;
    }

    public static void main(String[] args) {
        MinimumAmountOfTimeToFillCups2335 solution = new MinimumAmountOfTimeToFillCups2335();
        System.out.println(solution.fillCups(new int[]{5,4,4}));
    }
}
