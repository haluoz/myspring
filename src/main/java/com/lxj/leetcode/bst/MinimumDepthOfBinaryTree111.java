package com.lxj.leetcode.bst;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定一个二叉树，找出其最小深度。
 * 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
 * 说明：叶子节点是指没有子节点的节点。
 * 示例 1：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：2
 * 示例 2：
 * 输入：root = [2,null,3,null,4,null,5,null,6]
 * 输出：5
 * @author lee
 */
public class MinimumDepthOfBinaryTree111 {

    public int minDepth(TreeNode root) {
        if (root == null){
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        int depth = 0;
        queue.add(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            depth++;
            int flag = 0;
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if (node.left != null){
                    queue.add(node.left);
                }
                if (node.right != null){
                    queue.add(node.right);
                }
                // 当左右孩子都为空的时候，说明是最低点的一层了，退出
                if (node.left == null && node.right == null){
                    flag = 1;
                    break;
                }
            }
            if (flag == 1){
                break;
            }
        }
        return depth;
    }

    public static void main(String[] args) {
        MinimumDepthOfBinaryTree111 solution = new MinimumDepthOfBinaryTree111();
        TreeNode root = new TreeNode(1);
//        root.left = new TreeNode(9);
        root.right = new TreeNode(2,new TreeNode(4), new TreeNode(3,new TreeNode(6),new TreeNode(5)));

        solution.minDepth(root);
    }
}
