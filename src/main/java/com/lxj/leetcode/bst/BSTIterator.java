package com.lxj.leetcode.bst;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author lee
 */
public class BSTIterator {

    private TreeNode cur;
    private Deque<TreeNode> stack;

    public BSTIterator(TreeNode root) {
        cur = root;
        stack = new LinkedList<TreeNode>();
    }

    public int next() {
        while (cur != null) {
            stack.push(cur);
            cur = cur.left;
        }
        cur = stack.pop();
        int ret = cur.val;
        cur = cur.right;
        return ret;
    }

    public boolean hasNext() {
        return cur != null || !stack.isEmpty();
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(7, new TreeNode(3), new TreeNode(15, new TreeNode(9), new TreeNode(20)));
        BSTIterator solution = new BSTIterator(root);
        System.out.println(solution.next());
        System.out.println(solution.next());
        System.out.println(solution.hasNext());
        System.out.println(solution.next());
        System.out.println(solution.hasNext());
        System.out.println(solution.next());
        System.out.println(solution.hasNext());
        System.out.println(solution.next());
        System.out.println(solution.hasNext());
    }
}
