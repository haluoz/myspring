package com.lxj.leetcode.bst;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lee
 */
public class IncreasingOrderSearchTree897 {

    private List<Integer> list = new ArrayList<>();
    private TreeNode root;

    public IncreasingOrderSearchTree897() {
        root = new TreeNode(5, new TreeNode(1), new TreeNode(7));
    }

    public TreeNode increasingBST(TreeNode root) {
        inOrder(root);
        TreeNode dummyNode = new TreeNode(-1);
        TreeNode currNode = dummyNode;
        for(Integer i = 0; i<list.size() ;i++){
            currNode.right = new TreeNode(list.get(i));
            currNode = currNode.right;
        }
        return dummyNode.right;
    }

    public void inOrder(TreeNode node){
        if(node == null){
            return;
        }
        inOrder(node.left);
        list.add(node.val);
        inOrder(node.right);
    }

    public static void main(String[] args) {
        IncreasingOrderSearchTree897 solution = new IncreasingOrderSearchTree897();
        solution.increasingBST(solution.root);
    }
}
