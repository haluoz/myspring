package com.lxj.leetcode.linkedlist;

/**
 * Given the head of a linked list, rotate the list to the right by k places.
 * Example 1:
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 * Example 2:
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 * @author lee
 */
public class RotateList {

    private ListNode root;

    public RotateList() {
        root = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
    }

    public ListNode rotateRight(ListNode head, int k) {
        if(head == null){
            return head;
        }
        int n = 1;
        ListNode cur = head;
        //获得链表的长度
        while(cur.next != null){
            cur = cur.next;
            n++;
        }
        //k 为 n 的倍数时，新链表将与原链表相同
        int add = n - k % n;
        if (add == n) {
            return head;
        }
        //末尾节点与头节点相连，形成环链表
        cur.next = head;
        //找到需要向前n次的节点
        while (add-- > 0) {
            cur = cur.next;
        }
        ListNode ret = cur.next;
        //断开环链表
        cur.next = null;
        return ret;
    }

    public static void main(String[] args) {
        RotateList solution = new RotateList();
        solution.rotateRight(solution.root, 4);
    }
}
