package com.lxj.leetcode.linkedlist;

/**
 * @author lee
 */
public class RemoveDuplicatesFromSortedListII82 {

    private ListNode root;

    public RemoveDuplicatesFromSortedListII82() {
        root = new ListNode(1);
        root.next = new ListNode(2);
        root.next.next = new ListNode(3,new ListNode(3));
        root.next.next.next.next = new ListNode(4,new ListNode(4, new ListNode(5)));
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null){
            return null;
        }
        ListNode dummyHead = new ListNode(-1,head);
        ListNode cur = dummyHead;
        while(cur.next != null && cur.next.next != null){
            if(cur.next.val == cur.next.next.val){
                int x = cur.next.val;
                //如果当前节点的下一个节点的值等于x删除，进入下次循环
                while (cur.next != null && cur.next.val == x) {
                    cur.next = cur.next.next;
                }
            }else{
                cur = cur.next;
            }
        }
        return dummyHead.next;
    }

    public int kthToLast(ListNode head, int k) {
         if(head == null){
             return 0;
         }
         int size = 1;
         ListNode count =  head;
         while(count.next !=null ){
             count = count.next;
             size ++;
         }
         int forward = size - k;
         ListNode cur =  head;
         for(int i =1; i< forward;i++){
             cur = cur.next;
         }
        return cur.val;
//        ListNode firstP = head, sencondP = head;
//        for (int i = 0; i < k; i++) {
//            firstP = firstP.next;
//        }
//        while(firstP != null){
//            firstP = firstP.next;
//            sencondP = sencondP.next;
//        }
//        return sencondP.val;
    }

    public static void main(String[] args) {
        RemoveDuplicatesFromSortedListII82 solution = new RemoveDuplicatesFromSortedListII82();
//        solution.deleteDuplicates(solution.root);
        solution.kthToLast(solution.root, 2);
    }

}
