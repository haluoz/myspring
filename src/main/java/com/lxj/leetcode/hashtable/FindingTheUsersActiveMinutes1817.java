package com.lxj.leetcode.hashtable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lee
 * @since 2023/1/20
 */
public class FindingTheUsersActiveMinutes1817 {

    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        HashMap<Integer, Set<Integer>> map = new HashMap<>();
        int n = logs.length;
        for (int i = 0; i < n; i++) {
            int id = logs[i][0];
            int time = logs[i][1];
            map.putIfAbsent(id, new HashSet<>());
            map.get(id).add(time);
        }
        int[] answers = new int[k];
        for (Set<Integer> value : map.values()) {
            int size = value.size();
            answers[size-1]++;
        }
        return  answers;
    }

    public static void main(String[] args) {
        FindingTheUsersActiveMinutes1817 solution = new FindingTheUsersActiveMinutes1817();
        int [][] logs = {{0,5},{1,2},{0,2},{0,5},{1,3}};
        solution.findingUsersActiveMinutes(logs,5);
    }
}
