package com.lxj.leetcode.string;

/**
 * @author lee
 * @since 2023/1/15
 */
public class MinMaxGame2293 {

    public int minMaxGame(int[] nums) {
        int n = nums.length;
        if(n == 1) return nums[0];
        int [] newNums = new int[n/2];
        while (n>1) {
            for (int i = 0; i < n / 2; i++) {
                if (i % 2 == 0) {
                    newNums[i] = Math.min(nums[2 * i], nums[2 * i + 1]);
                } else {
                    newNums[i] = Math.max(nums[2 * i], nums[2 * i + 1]);
                }
            }
            n /= 2;
            nums = newNums;
        }
        return newNums[0];
    }

    public static void main(String[] args) {
        MinMaxGame2293 solution = new MinMaxGame2293();
        solution.minMaxGame(new int[]{999,939,892,175,114,464,850,107});
    }
}
