package com.lxj.leetcode.greedy;

/**
 * @author lee
 * @since 2023/1/26
 */
public class SmallestStringWithAGivenNumericValue1663 {

    public String getSmallestString(int n, int k) {
        StringBuilder sb = new StringBuilder();
        // for n times
        for (int i = 1; i <= n; i++) {
            //<=0选择字符a >=0 计算对应字符
            int maxLetter = Math.max(1, k-(n-i)*26);
            k -= maxLetter;
            sb.append((char)('a' + maxLetter -1));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        SmallestStringWithAGivenNumericValue1663 solution = new SmallestStringWithAGivenNumericValue1663();
        System.out.println(solution.getSmallestString(3, 27));
    }
}
