package com.lxj.leetcode.greedy;

import java.util.Arrays;

/**
 * @author lee
 * @since 2023/2/4
 */
public class MaximumNumberOfConsecutiveValuesYouCanMake1798 {

    public int getMaximumConsecutive(int[] coins) {
        int ans =1;
        Arrays.sort(coins);
        for(int i : coins){
            if (i > ans) {
                break;
            }
            ans += i;
        }
        return ans;
    }

    public static void main(String[] args) {
        MaximumNumberOfConsecutiveValuesYouCanMake1798 solution = new MaximumNumberOfConsecutiveValuesYouCanMake1798();
        solution.getMaximumConsecutive(new int[]{1,4,10,3,1});
    }
}
