package com.lxj.leetcode.array;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author lee
 * @since 2023/2/19
 */
public class MergeTwo2DArraysBySummingValues6362 {
    public int[][] mergeArrays(int[][] nums1, int[][] nums2) {
        Map<Integer, Integer> map = new TreeMap<>();
        for (int[] ints : nums2) {
            int key = ints[0];
            int value = ints[1];
            map.putIfAbsent(key, value);
        }
        for (int[] ints : nums1) {
            int key = ints[0];
            int value = ints[1];
            Integer integer = map.getOrDefault(key, 0);
            map.put(key, value+integer);
        }
        int n = map.size();
        int[][] ans = new int[n][2];
        int i =0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            int [] subAns = {key, value};
            ans[i++] = subAns;
        }
        return ans;
    }

    public static void main(String[] args) {
        MergeTwo2DArraysBySummingValues6362 solution = new MergeTwo2DArraysBySummingValues6362();
        int [][] num1 = {{2,4},{3,6},{5,5}};
        int [][] num2 = {{1,3},{4,3}};
        solution.mergeArrays(num1, num2);
    }
}
